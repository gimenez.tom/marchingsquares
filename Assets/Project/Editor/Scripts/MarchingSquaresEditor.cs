using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using ImageMagick;

public class MarchingSquaresEditor
{
	public static MarchingSquaresResult Execute(in MarchingSquaresParameters parameters)
	{
		NativeArray<int> contourLinesTable = new NativeArray<int>(new int[]
		{
			-1, -1, -1, -1, -1, -1, -1, -1,
			0, 1, 0, 3, -1, -1, -1, -1,
			1, 0, 1, 2, -1, -1, -1, -1,
			0, 3, 1, 2, -1, -1, -1, -1,

			2, 1, 2, 3, -1, -1, -1, -1,
			0, 1, 0, 3, 2, 1, 2, 3,
			1, 0, 2, 3, -1, -1, -1, -1,
			0, 3, 2, 3, -1, -1, -1, -1,

			3, 0, 3, 2, -1, -1, -1, -1,
			0, 1, 3, 2, -1, -1, -1, -1,
			1, 0, 1, 2, 3, 0, 3, 2,
			3, 2, 1, 2, -1, -1, -1, -1,

			3, 0, 2, 1, -1, -1, -1, -1,
			0, 1, 2, 1, -1, -1, -1, -1,
			3, 0, 1, 0, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1,
		}, Allocator.Persistent);

		Grid<int> grid = new Grid<int>();

		grid.width = parameters.width;
		grid.height = parameters.height;
		grid.values = new NativeArray<int>(grid.width * grid.height, Allocator.Persistent);
		FillGridWithRandomValues(ref grid, parameters.maxValue, parameters.threshold, parameters.seed);

		Grid<int> cellGrid = new Grid<int>();
		cellGrid.width = grid.width - 1;
		cellGrid.height = grid.height - 1;
		cellGrid.values = new NativeArray<int>(cellGrid.width * cellGrid.height, Allocator.Persistent);

		ComputeCellShapeIndicesJob computeCellShapeIndicesJob = new ComputeCellShapeIndicesJob()
		{
			grid = grid,
			cellGrid = cellGrid
		};

		JobHandle computeCellShapeIndicesJobHandle = computeCellShapeIndicesJob.Schedule();

		computeCellShapeIndicesJobHandle.Complete();

		MarchingSquaresResult result = new MarchingSquaresResult();
		result.points = new List<Vector2>(cellGrid.width * cellGrid.height * 4);
		result.segments = new List<int>(cellGrid.width * cellGrid.height * 2);

		Vector2[] currentPoints = new Vector2[2];
		for (int i = 0; i < cellGrid.height; ++i)
		{
			for (int j = 0; j < cellGrid.width; ++j)
			{
				int cellValue = cellGrid.values[i * cellGrid.width + j];
				NativeArray<int> contour = contourLinesTable.GetSubArray(cellValue * 8, 4);
				if (contour[0] == -1)
				{
					continue;
				}

				ComputePoints(ref contour, j, i, parameters.cellSize, currentPoints);
				result.points.AddRange(currentPoints);
				result.segments.Add(result.segments.Count);
				result.segments.Add(result.segments.Count);

				contour = contourLinesTable.GetSubArray(cellValue * 8 + 4, 4);
				if (contour[0] == -1)
				{
					continue;
				}

				ComputePoints(ref contour, j, i, parameters.cellSize, currentPoints);
				result.points.AddRange(currentPoints);
				result.segments.Add(result.segments.Count);
				result.segments.Add(result.segments.Count);
			}
		}

		cellGrid.values.Dispose();
		grid.values.Dispose();
		contourLinesTable.Dispose();

		return result;
	}

	public static MarchingSquaresResult Execute(
		in MarchingSquaresParameters parameters, in PerlinNoiseParameters perlinNoiseParameters
		)
	{
		NativeArray<int> contourLinesTable = new NativeArray<int>(new int[]
		{
			-1, -1, -1, -1, -1, -1, -1, -1,
			0, 1, 0, 3, -1, -1, -1, -1,
			1, 0, 1, 2, -1, -1, -1, -1,
			0, 3, 1, 2, -1, -1, -1, -1,

			2, 1, 2, 3, -1, -1, -1, -1,
			0, 1, 0, 3, 2, 1, 2, 3,
			1, 0, 2, 3, -1, -1, -1, -1,
			0, 3, 2, 3, -1, -1, -1, -1,

			3, 0, 3, 2, -1, -1, -1, -1,
			0, 1, 3, 2, -1, -1, -1, -1,
			1, 0, 1, 2, 3, 0, 3, 2,
			3, 2, 1, 2, -1, -1, -1, -1,

			3, 0, 2, 1, -1, -1, -1, -1,
			0, 1, 2, 1, -1, -1, -1, -1,
			3, 0, 1, 0, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1,
		}, Allocator.Persistent);

		Grid<float> perlinNoise = PerlinNoise.Compute(in perlinNoiseParameters);
		Grid<int> grid = new Grid<int>()
		{
			width = perlinNoise.width,
			height = perlinNoise.height,
			values = new NativeArray<int>(perlinNoise.values.Length, Allocator.Persistent)
		};

		int valueCount = perlinNoise.values.Length;
		for (int i = 0; i < valueCount; ++i)
		{
			int v = (int)((perlinNoise.values[i] / 2.0f + 0.5f) * parameters.maxValue);
			grid.values[i] = v >= parameters.threshold ? 1 : 0;
		}

		perlinNoise.Dispose();

		Grid<int> cellGrid = new Grid<int>();
		cellGrid.width = grid.width - 1;
		cellGrid.height = grid.height - 1;
		cellGrid.values = new NativeArray<int>(cellGrid.width * cellGrid.height, Allocator.Persistent);

		ComputeCellShapeIndicesJob computeCellShapeIndicesJob = new ComputeCellShapeIndicesJob()
		{
			grid = grid,
			cellGrid = cellGrid
		};

		JobHandle computeCellShapeIndicesJobHandle = computeCellShapeIndicesJob.Schedule();

		computeCellShapeIndicesJobHandle.Complete();

		MarchingSquaresResult result = new MarchingSquaresResult();
		result.points = new List<Vector2>(cellGrid.width * cellGrid.height * 4);
		result.segments = new List<int>(cellGrid.width * cellGrid.height * 2);

		Vector2[] currentPoints = new Vector2[2];
		for (int i = 0; i < cellGrid.height; ++i)
		{
			for (int j = 0; j < cellGrid.width; ++j)
			{
				int cellValue = cellGrid.values[i * cellGrid.width + j];
				NativeArray<int> contour = contourLinesTable.GetSubArray(cellValue * 8, 4);
				if (contour[0] == -1)
				{
					continue;
				}

				ComputePoints(ref contour, j, i, parameters.cellSize, currentPoints);
				result.points.AddRange(currentPoints);
				result.segments.Add(result.segments.Count);
				result.segments.Add(result.segments.Count);

				contour = contourLinesTable.GetSubArray(cellValue * 8 + 4, 4);
				if (contour[0] == -1)
				{
					continue;
				}

				ComputePoints(ref contour, j, i, parameters.cellSize, currentPoints);
				result.points.AddRange(currentPoints);
				result.segments.Add(result.segments.Count);
				result.segments.Add(result.segments.Count);
			}
		}

		cellGrid.values.Dispose();
		grid.values.Dispose();
		contourLinesTable.Dispose();

		return result;
	}

	public static void ExecuteAndDrawToPNG(ref MarchingSquaresParameters parameters)
	{
		MarchingSquaresResult result = Execute(in parameters);

		using (MagickImage image = new MagickImage(
			MagickColor.FromRgb(0xFF, 0xFF, 0xFF),
			(parameters.width - 1) * (int)parameters.cellSize,
			(parameters.height - 1) * (int)parameters.cellSize)
			)
		{
			int segmentCount = result.segments.Count / 2;
			for (int i = 0; i < segmentCount; ++i)
			{
				Vector2 startPoint = result.points[result.segments[i * 2]];
				Vector2 endPoint = result.points[result.segments[i * 2 + 1]];
				new Drawables()
					.Line(startPoint.x, startPoint.y, endPoint.x, endPoint.y)
					.Draw(image);
			}

			image.Flip();

			image.Write($"{Application.dataPath}/marching_squares_threshold_{parameters.threshold}.png");
		}
	}

	private static void ComputePoints(ref NativeArray<int> contour, int j, int i, float cellSize, Vector2[] points)
	{
		float x = j * cellSize;
		float y = i * cellSize;

		float startOffsetX = ((contour[0] == 0 || contour[0] == 3 ? 0.0f : cellSize) + (contour[1] == 0 || contour[1] == 3 ? 0.0f : cellSize)) / 2;
		points[0].x = x + startOffsetX;
		float startOffsetY = ((contour[0] < 2 ? 0.0f : cellSize) + (contour[1] < 2 ? 0.0f : cellSize)) / 2;
		points[0].y = y + startOffsetY;

		float endOffsetX = ((contour[2] == 0 || contour[2] == 3 ? 0.0f : cellSize) + (contour[3] == 0 || contour[3] == 3 ? 0.0f : cellSize)) / 2;
		points[1].x = x + endOffsetX;
		float endOffsetY = ((contour[2] < 2 ? 0.0f : cellSize) + (contour[3] < 2 ? 0.0f : cellSize)) / 2;
		points[1].y = y + endOffsetY;
	}

	private static void FillGridWithRandomValues(ref Grid<int> grid, int maxValue, int threshold, int seed)
	{
		Random.State previousRandomState = Random.state;
		Random.InitState(seed);
		{
			for (int i = 0; i < grid.height; ++i)
			{
				for (int j = 0; j < grid.width; ++j)
				{
					int randomValue = (int)(Random.value * maxValue);
					grid.values[i * grid.width + j] = randomValue >= threshold ? 1 : 0;
				}
			}
		}
		Random.state = previousRandomState;
	}
}
