using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

public class MarchingSquaresEditorWindow : EditorWindow
{
	[SerializeField]
	private MarchingSquaresParameters _parameters = new MarchingSquaresParameters();

	[SerializeField]
	private PerlinNoiseParameters _perlinParameters = new PerlinNoiseParameters();

	private MarchingSquaresResult _result = new MarchingSquaresResult();

	IntegerField _intWidth = null;
	IntegerField _intHeight = null;
	FloatField _floatCellSize = null;
	IntegerField _intMaxValue = null;
	IntegerField _intThreshold = null;
	IntegerField _intFrequency = null;
	IntegerField _intSeed = null;

	public MarchingSquaresEditorWindow()
	{
		_parameters.width = 64;
		_parameters.height = 64;
		_parameters.cellSize = 64.0f;
		_parameters.maxValue = 100;
		_parameters.threshold = 50;
		_parameters.seed = 1;

		_perlinParameters.width = 64;
		_perlinParameters.height = 64;
		_perlinParameters.frequency = 8;
		_perlinParameters.seed = 1;
	}

	#region Methods
	[MenuItem("Window/Marching Squares Editor")]
	public static void ShowExample()
	{
		MarchingSquaresEditorWindow wnd = GetWindow<MarchingSquaresEditorWindow>();
		wnd.titleContent = new GUIContent("Marching Squares Editor");
	}

	public void CreateGUI()
	{
		// Each editor window contains a root VisualElement object
		VisualElement root = rootVisualElement;

		// A stylesheet can be added to a VisualElement.
		// The style will be applied to the VisualElement and all of its children.
		var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Project/Editor/Scripts/MarchingSquaresEditorWindow.uss");

		// Import UXML
		var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Project/Editor/Scripts/MarchingSquaresEditorWindow.uxml");
		VisualElement template = visualTree.Instantiate();
		template.styleSheets.Add(styleSheet);
		root.Add(template);

		// Settings

		_intWidth = root.Q<IntegerField>("int-width");
		_intWidth.value = _parameters.width;
		_intWidth.RegisterValueChangedCallback(evt =>
		{
			_parameters.width = evt.newValue > 0 ? evt.newValue : 1;
			_perlinParameters.width = evt.newValue > 0 ? evt.newValue : 1;
			_intWidth.value = _parameters.width;

			Execute();
		});

		_intHeight = root.Q<IntegerField>("int-height");
		_intHeight.value = _parameters.height;
		_intHeight.RegisterValueChangedCallback(evt =>
		{
			_parameters.height = evt.newValue > 0 ? evt.newValue : 1;
			_perlinParameters.height = evt.newValue > 0 ? evt.newValue : 1;
			_intHeight.value = _parameters.height;

			Execute();
		});

		_floatCellSize = root.Q<FloatField>("float-cellsize");
		_floatCellSize.value = _parameters.cellSize;
		_floatCellSize.RegisterValueChangedCallback(evt =>
		{
			_parameters.cellSize = evt.newValue > 0 ? evt.newValue : 1.0f;
			_floatCellSize.value = _parameters.cellSize;

			Execute();
		});

		_intMaxValue = root.Q<IntegerField>("int-maxvalue");
		_intMaxValue.value = _parameters.maxValue;
		_intMaxValue.RegisterValueChangedCallback(evt =>
		{
			_parameters.maxValue = evt.newValue >= 0 ? evt.newValue : 0;
			_intMaxValue.value = _parameters.maxValue;
			if (_parameters.threshold > _parameters.maxValue)
			{
				_parameters.threshold = _parameters.maxValue;
				_intThreshold.value = _parameters.threshold;
			}

			Execute();
		});

		_intThreshold = root.Q<IntegerField>("int-threshold");
		_intThreshold.value = _parameters.threshold;
		_intThreshold.RegisterValueChangedCallback(evt =>
		{
			_parameters.threshold = evt.newValue >= 0 ? evt.newValue : 0;
			if (_parameters.threshold > _parameters.maxValue) _parameters.threshold = _parameters.maxValue;
			_intThreshold.value = _parameters.threshold;

			Execute();
		});

		_intFrequency = root.Q<IntegerField>("int-frequency");
		_intFrequency.value = (int)_perlinParameters.frequency;
		_intFrequency.RegisterValueChangedCallback(evt =>
		{
			_perlinParameters.frequency = (uint)evt.newValue;

			Execute();
		});

		_intSeed = root.Q<IntegerField>("int-seed");
		_intSeed.value = _parameters.seed;
		_intSeed.RegisterValueChangedCallback(evt =>
		{
			int newSeed = evt.newValue > 0 ? evt.newValue : 1;
			_parameters.seed = newSeed;
			_perlinParameters.seed = (uint)newSeed;

			_intSeed.value = newSeed;

			Execute();
		});

		// Actions

		Button btnExecute = root.Q<Button>("btn-execute");
		btnExecute.clicked += Execute;

		Button btnPerlin = root.Q<Button>("btn-perlin");
		btnPerlin.clicked += ExecutePerlin;


		Execute();
	}

	public void OnFocus()
	{
		SceneView.duringSceneGui -= OnSceneGUI;
		SceneView.duringSceneGui += OnSceneGUI;
	}

	public void OnDestroy()
	{
		SceneView.duringSceneGui -= OnSceneGUI;
	}

	#region Callbacks
	private void Execute()
	{
		//_result = MarchingSquaresEditor.Execute(in _parameters);
		_result = MarchingSquaresEditor.Execute(in _parameters, in _perlinParameters);
		SceneView.RepaintAll();
	}

	private void ExecutePerlin()
	{
		_perlinParameters.width = _parameters.width;
		_perlinParameters.height = _parameters.height;
		_perlinParameters.seed = (uint)_parameters.seed;

		Grid<float> perlin = PerlinNoise.Compute(in _perlinParameters);

		int pixelCount = perlin.width * perlin.height;
		Color[] pixels = new Color[pixelCount];
		for (int i = 0; i < pixelCount; ++i)
		{
			float v = perlin.values[i] / 2.0f + 0.5f;
			pixels[i] = new Color(v, v, v);
		}

		Texture2D texture = new Texture2D(perlin.width, perlin.height, TextureFormat.RGBA32, false);
		texture.SetPixels(pixels, 0);
		texture.Apply();

		byte[] rawTextureData = texture.EncodeToTGA();
		File.WriteAllBytes("Assets/perlin.tga", rawTextureData);
		AssetDatabase.Refresh();

		perlin.values.Dispose();

		SceneView.RepaintAll();
	}
	#endregion Callbacks

	#region OnSceneGUI
	private void OnSceneGUI(SceneView sceneView)
	{
		if (_result.points == null || _result.segments == null)
		{
			return;
		}

		Color previousColor = Handles.color;
		Handles.color = Color.black;
		int segmentCount = _result.segments.Count / 2;
		for (int i = 0; i < segmentCount; ++i)
		{
			Vector2 startPoint = _result.points[_result.segments[i * 2]];
			Vector2 endPoint = _result.points[_result.segments[i * 2 + 1]];
			Handles.DrawLine(new Vector3(startPoint.x, 0.0f, startPoint.y), new Vector3(endPoint.x, 0.0f, endPoint.y));
		}
		Handles.color = previousColor;
	}
	#endregion OnSceneGUI
	#endregion Methods
}