using System.Collections.Generic;
using UnityEngine;

public struct MarchingSquaresResult
{
	public List<Vector2> points;
	public List<int> segments;
}
