using UnityEngine;
using Unity.Collections;
using Unity.Mathematics;

public static class PerlinNoise
{
	public static Grid<float> Compute(in PerlinNoiseParameters parameters)
	{
		if (parameters.frequency > parameters.width || parameters.frequency > parameters.height || parameters.frequency == 0.0f)
		{
			return new Grid<float>()
			{
				width = 0,
				height = 0,
				values = new NativeArray<float>(0, Allocator.Persistent)
			};
		}

		int gradientWidth = parameters.width % parameters.frequency == 0 ? parameters.width / (int)parameters.frequency + 1 : (parameters.width / (int)parameters.frequency) + 2;
		int gradientHeight = parameters.height % parameters.frequency == 0 ? parameters.height / (int)parameters.frequency + 1 : (parameters.height / (int)parameters.frequency) + 2;
		Grid<float2> gradients = ComputeGradients(gradientWidth, gradientHeight, parameters.seed);

		Grid<float> perlin = ComputePerlin(in gradients, in parameters);

		gradients.values.Dispose();

		return perlin;
	}

	private static Grid<float2> ComputeGradients(int width, int height, uint seed)
	{
		Grid<float2> gradients = new Grid<float2>();
		gradients.width = width;
		gradients.height = height;
		gradients.values = new NativeArray<float2>(width * height, Allocator.Persistent);

		Unity.Mathematics.Random rnd = new Unity.Mathematics.Random(seed);
		for (int i = 0; i < width * height; ++i)
		{
			gradients.values[i] = rnd.NextFloat2Direction();
		}

		return gradients;
	}

	private static Grid<float> ComputePerlin(in Grid<float2> gradients, in PerlinNoiseParameters parameters)
	{
		Grid<float> grid = new Grid<float>();
		grid.width = parameters.width;
		grid.height = parameters.height;
		grid.values = new NativeArray<float>(grid.width * grid.height, Allocator.Persistent);

		Unity.Mathematics.Random rnd = new Unity.Mathematics.Random(parameters.seed);
		float2 position = new float2();
		for (int i = 0; i < grid.height; ++i)
		{
			position.y = i / (float)parameters.frequency;
			for (int j = 0; j < grid.width; ++j)
			{
				position.x = j / (float)parameters.frequency;

				grid.values[i * grid.width + j] = ComputeNoise(in gradients, position);
			}
		}

		return grid;
	}

	private static float ComputeNoise(in Grid<float2> gradients, float2 position)
	{
		// Determine grid cell coordinates
		int x0 = (int)position.x;
		int x1 = x0 + 1;
		int y0 = (int)position.y;
		int y1 = y0 + 1;

		// Determine interpolation weights
		// Could also use higher order polynomial/s-curve here
		float smoothX = Fade(position.x - x0);
		float smoothY = Fade(position.y - y0);

		// Interpolate between grid point gradients
		float n0, n1, ix0, ix1, noise;
		n0 = DotGridGradient(in gradients, new float2(x0, y0), in position);
		n1 = DotGridGradient(in gradients, new float2(x1, y0), position);
		ix0 = Lerp(n0, n1, smoothX);
		n0 = DotGridGradient(in gradients, new float2(x0, y1), position);
		n1 = DotGridGradient(in gradients, new float2(x1, y1), position);
		ix1 = Lerp(n0, n1, smoothX);
		noise = Lerp(ix0, ix1, smoothY);

		return noise;
	}

	private static float DotGridGradient(in Grid<float2> gradients, float2 positionOnGrid, in float2 position)
	{
		// Compute the distance vector
		float2 distance = position - positionOnGrid;

		// Compute the dot-product
		float2 gradient = gradients.values[(int)(positionOnGrid.y * gradients.width + positionOnGrid.x)];
		return math.dot(distance, gradient);
	}

	private static float Lerp(float a0, float a1, float w)
	{
		return (a1 - a0) * w + a0;
	}

	private static float Fade(float t)
	{
		return ((6 * t - 15) * t + 10) * t * t * t;
	}
}
