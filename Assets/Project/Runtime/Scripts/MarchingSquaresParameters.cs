using System;

[Serializable]
public struct MarchingSquaresParameters
{
	public int width;
	public int height;
	public float cellSize;
	public int maxValue;
	public int threshold;
	public int seed;
}
