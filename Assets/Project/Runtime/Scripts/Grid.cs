using System;
using Unity.Collections;

public struct Grid<T> : IDisposable where T : unmanaged
{
	public NativeArray<T> values;
	public int width;
	public int height;

	public void Dispose()
	{
		values.Dispose();
	}
}

