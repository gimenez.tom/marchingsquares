public struct PerlinNoiseParameters
{
	public int width;
	public int height;
	public uint frequency;
	public uint seed;
}
