using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

[BurstCompile]
public struct ComputeCellShapeIndicesJob : IJob
{
	[ReadOnly]
	public Grid<int> grid;
	public Grid<int> cellGrid;

	public void Execute()
	{
		for (int i = 0; i < cellGrid.height; ++i)
		{
			for (int j = 0; j < cellGrid.width; ++j)
			{
				int p0 = grid.values[i * grid.width + j];
				int p1 = grid.values[i * grid.width + j + 1];
				int p2 = grid.values[(i + 1) * grid.width + j + 1];
				int p3 = grid.values[(i + 1) * grid.width + j];

				int cellValue = p0;
				cellValue |= p1 << 1;
				cellValue |= p2 << 2;
				cellValue |= p3 << 3;

				cellGrid.values[i * cellGrid.width + j] = cellValue;
			}
		}
	}
}
